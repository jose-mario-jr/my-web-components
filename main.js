import { StarRater } from "./ratingcomponent/StarRater.js"

document.addEventListener("DOMContentLoaded", () => {
  const raterComp = new StarRater()
  raterComp.setAttribute("data-rating", "4")
  const raterComp2 = new StarRater()
  raterComp2.setAttribute("data-rating", "1")

  document.body.appendChild(raterComp)
  document.body.appendChild(raterComp2)
})
