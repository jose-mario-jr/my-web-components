const template = document.getElementById("t-star-rater")

class StarRater extends HTMLElement {
  constructor() {
    super()
    this.appendChild(template.content.cloneNode(true))
    this.stars = this.querySelectorAll(".star")
    this.dataRating = this.getAttribute("data-rating")
  }

  connectedCallback() {
    this.stars.forEach((star) => {
      star.addEventListener("click", (ev) => {
        this.stars.forEach((star) => star.classList.remove("hov"))
        this.setAttribute("data-rating", ev.target.getAttribute("data-value"))
      })
      star.addEventListener("mouseover", (ev) =>
        this.paintStars(ev.target.getAttribute("data-value"), "hov")
      )
    })

    this.addEventListener("mouseout", (ev) => {
      this.stars.forEach((star) => star.classList.remove("hov"))
    })
  }

  static get observedAttributes() {
    return ["data-rating"]
  }

  attributeChangedCallback(name, old, newV) {
    this.stars.forEach((star) => {
      newV >= star.getAttribute("data-value")
        ? star.classList.add("fill")
        : star.classList.remove("fill")
    })
  }

  paintStars(threshold, className) {
    this.stars.forEach((star) => {
      threshold >= star.getAttribute("data-value")
        ? star.classList.add(className)
        : star.classList.remove(className)
    })
  }
}
customElements.define("star-rater", StarRater)

export { StarRater }
